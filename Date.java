/**
 * A class representing a Date object.<br /> 
 * <br />
 * For example:
 * <pre>
 *   Date today = new Date();
 *   Date.print();
 * </pre>
 * This work has no copyright.
 * @author 5153
 * @version 0.9, 28 Sept 2017
 */
public class Date {
	private int month;
	private int day;
	private int year;
	
	/**
	* Class constructor, 
	*/
	public Date() {
		readData();
	}
	
	/**
	 * Prints questions to stdout asking the user to input 
	 *  instance variable data.
	 */
	private void readData() {
		System.out.printf("%7s Please input the month: ", " ");
		month = Utilities.scanner.nextInt();
		Utilities.scanner.nextLine();
		
		System.out.printf("%7s Please input the day: ", " ");
		day = Utilities.scanner.nextInt();
		Utilities.scanner.nextLine();
		
		System.out.printf("%7s Please input the year: ", " ");
		year = Utilities.scanner.nextInt();
		Utilities.scanner.nextLine();
	}
	
	/**
	* Prints the Date object data in the following format: 
	* month/day/year.
	*/
	public void print() {
		System.out.print("" + month + "/" + day + "/" + year);
	}
}
