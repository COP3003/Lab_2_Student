import java.util.Scanner;

/**
* Creates a public Scanner object to be used by all objects in this package.
*/
public class Utilities {
	public final static Scanner scanner = new Scanner(System.in);
}
