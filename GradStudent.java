/**
 * A class representing a Graduate Student.<br /> 
 * <br />
 * For example:
 * <pre>
 *   GradStudent gradStudent = new GradStudent();
 *   gradStudent.print();
 * </pre>
 * This work has no copyright.
 * @author 5153
 * @version 0.9, 28 Sept 2017
 */
public class GradStudent extends Student {
	private String researchTopic;
	private String advisor;
	
	/**
	 * Class constructor.
	 */
	public GradStudent() {
		readData();
	}
	
	/**
	* Follows Student.readData and prints more questions to stdout asking  
	*  the user to input instance variable data.<br /><br />
	*  
	* Question output varies by gender. Input is not validated.
	*/
	private void readData() {
		System.out.printf("Please input %s research topic:", 
							(gender == "m".charAt(0))?"his":"her");
		researchTopic = Utilities.scanner.nextLine();
		
		System.out.printf("Please input %s research advisor:", 
				(gender == "m".charAt(0))?"his":"her");
		advisor = Utilities.scanner.nextLine();
	}
	
	/**
	 * {@inheritDoc}
	 * Then prints a report of the the GradStudent object.
	 */
	@Override
	public void print() {
		super.print(); // Prints Student report.
		
		System.out.printf("\n%s research topic is %s.\n", 
				(gender == "m".charAt(0))?"His":"Her", researchTopic);
		System.out.printf("%s advisor is %s.\n", 
				(gender == "m".charAt(0))?"His":"Her", advisor);
	}
}
