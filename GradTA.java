/**
 * A class representing a Graduate Student with a TA position.<br /> 
 * <br />
 * For example:
 * <pre>
 *   GradTA taStudent = new GradTA();
 *   taStudent.print();
 * </pre>
 * This work has no copyright.
 * @author 5153
 * @version 0.9, 28 Sept 2017
 */
public class GradTA extends GradStudent {
	private String taCourse;
	private String taInstructor;
	private Date hireDate; // month/day/year
	
	/**
	 * Class constructor.
	 */
	public GradTA() {
		readData();
	}
	
	/**
	* Follows GradStudent method readData and prints more questions to stdout 
	*  asking the user to input instance variable data.<br /><br />
	*  
	* Question output varies by gender. Input is not validated.
	*/
	private void readData() {
		System.out.printf("Please input %s TA course:", 
							(gender == "m".charAt(0))?"his":"her");
		taCourse = Utilities.scanner.nextLine();
		
		System.out.printf("Please input %s TA instructor:", 
				(gender == "m".charAt(0))?"his":"her");
		taInstructor = Utilities.scanner.nextLine();
		
		System.out.printf("Please input %s hire date:\n", 
							(gender == "m".charAt(0))?"his":"her");
		hireDate = new Date();
	}
	
	/**
	 * {@inheritDoc}
	 * Then prints a report of the the GradTA object.
	 */
	@Override
	public void print() {
		super.print(); // Prints GradStudent report.
		
		System.out.printf("%s TA course is %s.\n", 
				(gender == "m".charAt(0))?"His":"Her", taCourse);
		System.out.printf("%s TA intstructor is %s.\n", 
				(gender == "m".charAt(0))?"His":"Her", taInstructor);
		System.out.printf("%s hire date is ", 
				(gender == "m".charAt(0))?"His":"Her");
		hireDate.print();
	}
	
	/**
	 * GradTA Test driver method
	 * 
	 * @param args not used
	 */
	public static void main(String args[]){
		GradTA ta = new GradTA();
		ta.print();
	}
}
