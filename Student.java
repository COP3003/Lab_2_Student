/**
 * A class representing a Student.<br /> 
 * <br />
 * For example:
 * <pre>
 *   Student firstStudent = new Student();
 *   firstStudent.print();
 * </pre>
 * This work has no copyright.
 * @author 5153
 * @version 0.9, 28 Sept 2017
 */
public class Student {
	private String name;    // "firstname lastname"
	private int ssn; 	    // social security number 
	private int numOfCourses;
	private Date birthDate; // month/day/year
	protected char gender;  // male (m) or female (f)
	
	/**
	 * Class constructor.
	 */
	public Student() {
		readData();
	}
	
	/**
	* Prints questions to stdout asking the user to input 
	*  instance variable data.
	*  
	* Some question output varies by gender. Input is not validated.
	*/
	private void readData() {
		System.out.print("Please input the name: ");
		name = Utilities.scanner.nextLine();
		
		System.out.print("Please input the ssn: ");
		ssn = Utilities.scanner.nextInt();
		Utilities.scanner.nextLine();
		
		System.out.print("Male/Female (m/f): ");
		gender = Utilities.scanner.nextLine().charAt(0);
		
		System.out.printf("How many courses %s is taking: ",
							(gender == "m".charAt(0))?"he":"she");
		numOfCourses = Utilities.scanner.nextInt();
		Utilities.scanner.nextLine();
		
		System.out.printf("Please input %s birth date:\n", 
				(gender == "m".charAt(0))?"his":"her");
		birthDate = new Date();
	}
	
	/**
	* Prints a report of the Student object. Some output varies by gender.
	*/
	public void print() {
		System.out.println("\nThe information you input was");
		System.out.printf("%s's ssn is %d.\n",name, ssn);
		System.out.printf("%s is taking %d courses.\n", 
						  (gender == "m".charAt(0))?"He":"She", numOfCourses);
		System.out.printf("%s birth date is ", 
				(gender == "m".charAt(0))?"His":"Her");
		birthDate.print();
	}
}
